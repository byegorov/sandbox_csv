'use strict';

const express = require('express');
const app = express();

const csv = require('./csv');

app.get('/', function (req, res) {
  csv.generate().then(data => {
    res.writeHead(200, {
      'Content-Type': 'text/csv',
      'Content-disposition': 'attachment;filename=example.csv',
      'Content-Length': data.length
    });
    res.end(new Buffer(data, 'binary'));
  });
});

app.listen(3000);
