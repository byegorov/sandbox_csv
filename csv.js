'use strict';

const Promise = require('bluebird');
const Json2csv = require('json2csv');
const json2csv = Promise.promisify(Json2csv);

var generate = () => {
  var fields = ['car', 'price', 'color'];
  var data = [
    {
      "car": "Audi",
      "price": 40000,
      "color": "blue"
    }, {
      "car": "BMW",
      "price": 35000,
      "color": "black"
    }, {
      "car": "Porsche",
      "price": 60000,
      "color": "green"
    }
  ];

  return json2csv({ data, fields }).catch(console.log);
};

module.exports = { generate };
